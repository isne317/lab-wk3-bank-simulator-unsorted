#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <vector>
#include "customer.h"
#include "cashier.h"

using namespace std;

void customerSwap(customer&, customer&);

int main()
{
    int MinCustomers, MaxCustomers, MinServiceTime, MaxServiceTime, NumCashiers;
    cout << "Input minimum customer count: ";
    cin >> MinCustomers;
    cout << "Input maximum customer count: ";
    cin >> MaxCustomers;
    cout << "Input minimum service time per customer: ";
    cin >> MinServiceTime;
    cout << "Input maximum service time per customer: ";
    cin >> MaxServiceTime;
    cout << "Input number of cashier: ";
    cin >> NumCashiers;
    //Fail save
    if(MinCustomers <= 0) {MinCustomers = 1;}
    if(MinCustomers > MaxCustomers) {MaxCustomers = MinCustomers;}
    if(MinServiceTime <= 0) {MinServiceTime = 1;}
    if(MinServiceTime > MaxServiceTime) {MaxServiceTime = MinServiceTime;}
    if(NumCashiers <= 0) {NumCashiers = 1;}
    //Initializer
    srand(time(0));
    vector<customer> A;
    vector<cashier> B;
    int n = (rand() % ((MaxCustomers - MinCustomers)+ 1)) + MinCustomers;
    cout << "-------------------------" << endl;
    for(int i = 0; i < NumCashiers; i++)
    {
        cashier temp(0);
        B.push_back(temp);
    }
    for(int i = 0; i < n; i++)
    {
        customer temp(MinServiceTime,MaxServiceTime,i);
        A.push_back(temp);
    }
    for(int i = 0; i < n-1; i++)
    {
        int ind = i;
        customer low(A[i]);
        for(int j = i; j < n; j++)
        {
            if(A[j] < low)
            {
                low = A[j];
                ind = j;  
            }
        }
        customerSwap(A[i], A[ind]);
    }
    //NEXT
    int time = 0;
    int timediff = 0;
    float wait = 0;
    int delay = 0;
    bool gotASlot = false;
    for(int i = 0; i < n; i++)
    {
        delay = 0;
        timediff = A[i].get_arr() - time;
        time = A[i].get_arr();
        gotASlot = false;
        for(int j = 0; j < NumCashiers; j++)
        {
            B[j].refresh(timediff);
            if(B[j].get_available() && !gotASlot)
            {
                gotASlot = true;
                wait += A[i].set_left(A[i].get_arr() + A[i].get_ser() + delay);
                B[j].work(A[i].get_ser());
            }
        }
        if(!gotASlot)
        {   
            int ind;
            int low = B[0].get_CD();
            for(int j = 0; j < NumCashiers; j++)
            {
                if(B[j].get_CD() < low)
                {
                    ind = j;
                    low = B[j].get_CD();   
                }
            }
            delay += low;
            time += low;
            for(int j = 0; j < NumCashiers; j++)
            {
                B[j].refresh(low);
                if(B[j].get_available() && !gotASlot)
                {
                    gotASlot = true;
                    wait += A[i].set_left(A[i].get_arr() + A[i].get_ser() + delay);
                    B[j].work(A[i].get_ser());
                }
            }
        }
    }
    //NEXT
    for(int i = 0; i < n-1; i++)
    {
        int ind = i;
        int low(A[i].get_tag());
        for(int j = i; j < n; j++)
        {
            if(A[j].get_tag() < low)
            {
                low = A[j].get_tag();
                ind = j;
            }
        }
        customerSwap(A[i], A[ind]);
    }
    float avgWait = wait / n;
    for(int i = 0; i < n; i++)
    {
        cout << "Customer " << setw(3) << i+1 << "| Arrive: " << setw(3) << A[i].get_arr() << "| Wait time: " << setw(3) << A[i].get_wait() << "| Left: " << setw(3) << A[i].get_lef() << endl;
    }
    cout << "-------------------------" << endl;
    cout << "Average wait time: " << setprecision(2) << avgWait << " minutes" << endl;
    system("PAUSE");
    return 0;
}

void customerSwap(customer& A, customer& B)
{
    customer temp(A);
    A = B;
    B = temp;
}
