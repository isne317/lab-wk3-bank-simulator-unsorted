class customer
{
public:
    customer() 
    {
         
    }
    customer(int min, int max, int ind) 
    {
        int diff = (max-min)+1;
        arrival = rand() % 240;
        service_time = (rand() % diff) + min;
        tag = ind;
	}
	customer(const customer &cus)
	{
		arrival = cus.get_arr();
		service_time = cus.get_ser();
		tag = cus.get_tag();
		wait = cus.get_wait();
		left = cus.get_lef();
	}
	int get_arr() const{ return arrival; }
	int get_ser() const{ return service_time; }
	int get_lef() const{ return left; }
	int get_wait() const{ return wait; }
	int get_tag() const{ return tag; }
	
	int set_left(int time) 
    { 
        left = time; 
        if(time - (arrival + service_time) > 0)
        {
            wait = time - (arrival + service_time);
        }
        else
        {
            wait = 0;  
        }
        return wait;
    }

	friend bool operator <(customer a, customer b)
	{
		if(a.arrival < b.arrival)
		{ 
            return true;
        }	
        else
        { 
            return false;
        }
	}
	friend bool operator >(customer a, customer b)
	{
		if(a.arrival > b.arrival)
		{ 
            return true;
        }	
        else
        {
            return false;
        }
	}
	customer operator =(customer& rhs)
	{
		arrival = rhs.get_arr();
		service_time = rhs.get_ser();
		tag = rhs.get_tag();
		wait = rhs.get_wait();
		left = rhs.get_lef();
		return *this;
	}
private:
	int arrival;
	int service_time;
	int left;
	int wait;
	int tag;
};
