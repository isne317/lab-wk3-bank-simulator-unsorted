class cashier
{
    public:
        
        cashier ()
        {
            
        }
        cashier (int any)
        {
            available = true;
            cooldown = 0; 
        }
        
        bool get_available() const{return available;}
        int get_CD() const {return cooldown;}
        
        void work(int tick)
        {
            available = false;
            cooldown = tick;
        }
        
        void refresh(int tick)
        {
            cooldown -= tick;
            if(cooldown <= 0)
            {
                available = true;  
            }
            else
            {
                available = false;   
            }
        }
        
    private:
        bool available;
        int cooldown;
};
